Elijah core-api
================

`core-api` is an `Initiative` to move common parts of the various `Elijah` branches
into a common place in order to be managed, developed and ultimately consumed in a 
big ball of most likely exuberant fluffiness.

`core-api` contains most of the (Java) interfaces and enums used in this line of 
derivatives.  This includes:

- `g` (interconnect)
- `lang` and `contexts`
- `ci` and `comp`


Characteristically stolen content (something about flattery)
-------------------------------------------------------------

This repository is an attempt to use git to maintain multiple parallel ports of 
[fluffy-umbrella][15] across multiple hosting providers, build tools and implementation
languages.

Eventually we wish to bring all the core apis into these branches, then combine them all
into The One True fluff-ball, whenceforth we shall be closer to our goal.

There are currently two branches:

- `elevated-potential`
- `congenial-robot`

I am using Gradle now out of laziness and good enough-ness, although it shouldn't matter
anyway as these things are just interfaces to be consumed later by whatever else.  Eventually,
there should be a process methodology automation unstructure (not non/dis) to make things go 
smooth under the right/correct conditions.  

See [Cypress][16] for inspiration, but not (necessarily) inception. 

> The goal is to come up with an arrangement that will help move changes between the ports easily.
> Whether that will mean some clever scheme allowing to merge things across or whether it will be 
> some form of commit cherry-picking remains to be seen.

All the available ports are treated as equal and therefore none of them is the master. Instead each port has its own branch:

| Main repo          | Dialect | Branch link    
|--------------------|:-------:|:------------------------------------
| elevated-potential |   yyy   | [elevated][7]                 
| congenial-robot    |   yyy   | [congenial][8]             
| xxx                |   yyy   | [stare-directly-into-the-light][4]  

[1]: https://github.com/elijah-team/goawaynothingtoseehere
[2]: https://github.com/elijah-team/goawaynothingtoseehere
[3]: https://github.com/elijah-team/goawaynothingtoseehere
[4]: https://github.com/elijah-team/goawaynothingtoseehere
[5]: https://github.com/elijah-team/goawaynothingtoseehere
[6]: https://github.com/elijah-team/goawaynothingtoseehere
[7]: https://gitlab.com/elijah-team/elijah-core-api/-/tree/congenial?ref_type=heads
[8]: https://gitlab.com/elijah-team/elijah-core-api/-/tree/elevated?ref_type=heads

There may be some use for the master branch eventually as we figure things out, but currently it will be kept empty.

### Strategy

* [ ] Pull all pertinent interfaces (`api`) into branches, leaving out the irrelevant
  * [X] elevated-potential  
  * [X] congenial-robot
  * [ ] persistent-pancake
  * [ ] [prolific-remnant][10]
  * [ ] pacer
  * [ ] fantastic-giggle
  * [ ] fluffy (this one likely not)
  * [ ] lgpl (this one likely not as well)
* [ ] Use magic to make it all work in a way only I can understand
  * The problem is the tooling on the bottom (javac)
  * Not above that (Java, Kotlin, Scala, Clojure, Frege, Xtend)
  * Not in the middle (gradle, maven, ...)
  * Or even above that (Eclipse, Idea)
  * Or the top (whatever custom thing you're using that maybe you share or you don't)
* [ ] Change everything in a couple months when I come up with a better idea
* [ ] Change everything in a couple more months when I come up with a different in some extremely subtle way idea
* [ ] 

[10]: https://github.com/elijah-team/prolific-remnant
[15]: https://github.com/elijah-team/fluffy-umbrella
[16]: https://github.com/CampSmalltalk/Cypress
